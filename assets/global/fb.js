function fb_login() {
	FB.login( function() {
		checkLoginState();
	}, { scope: 'email,public_profile' } );
}
function statusChangeCallback(response) {
	console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
		// Logged into your app and Facebook.
		testAPI();
		// window.location.replace('./fbapp/login-callback.php');
		$.get('index.php?/home/login_fbcheck?name='+name,function(data) {
			// window.open("index.php?home/main", "_self");
			console.log(data);
		});

    } else if (response.status === 'not_authorized') {
		  // The person is logged into Facebook, but not your app.
    } else {
      	// The person is not logged into Facebook, so we're not sure if
      	// they are logged into this app or not.
    }
}

function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
}

window.fbAsyncInit = function() {
	FB.init({
		appId      : '752362361785527',
		cookie     : true,  // enable cookies to allow the server to access 
							// the session
		xfbml      : true,  // parse social plugins on this page
		version    : 'v2.5' // use any version
	});
};

// Load the SDK asynchronously
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function testAPI(){
	FB.api('/me?fields=name,email', function(response){
		if(response && !response.error){
			var name = response.name;
			$.get('index.php?/home/login_fbcheck?name='+name,function(data) {
				window.open("index.php?home/main", "_self");
				// console.log(data);
			});
		}
	})
}
