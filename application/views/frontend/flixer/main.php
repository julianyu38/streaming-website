<link media="screen" rel="stylesheet" href="//d0.tubitv.com/web/dist/main.a707ba1915e85b7fff99539b4a9a7e88.css"/>
<link media="screen" rel="stylesheet" href="<?php echo base_url() . 'assets/frontend/flixer/custom.css'?>"/>

<body data-reactroot="">
    <div id="content">
        <div id="app" class="AlOn7LvsqR" data-reactroot="">
            <div class="KBrro77I6M">
                <div class="_3Za8EybvuM"></div>
            </div>
            <div class="_3Qo9rgvWn-">
                <header class="U7IH3nT-xT RwMWOeGUEX">
                    <div class="pcWvID-kmo"></div>
                    <div class="Container _3JvwPHauvB">
                        <div class="_1sZ9qmLFG_">
                            <div class="_2v5vy4xZMA">
                                <div class="Ajer5aIfN6" rel="nofollow">
                                    <span class="pv50RvrNmq"></span>
                                </div>
                                <div class="_1JO9cOSNhq hide">
                                    <div class="_3IIDEoM0HV"><div>
                                    <div class="_3EgjJ39d9J">
                                        <section class="yOpR1UpemN _3o10ZDUXhE">
                                            <svg class="_2rWrRtG6JR _20cwlyjJGT" preserveAspectRatio="xMidYMid meet" viewBox="0 0 18.07 18.07" style="fill: currentcolor;">
                                                <path fill="currentColor" d="M7.5,13A5.5,5.5,0,1,0,2,7.5,5.5,5.5,0,0,0,7.5,13Zm4.55.46A7.5,7.5,0,1,1,13.46,12l4.31,4.31a1,1,0,1,1-1.41,1.41Z"></path>
                                            </svg>
                                            <form>
                                                <input class="_31SfFjM4T0" type="search" required="" placeholder="Search" value="">
                                            </form>
                                            <svg class="_2rWrRtG6JR _2se6dxbH_B" preserveAspectRatio="xMidYMid meet" viewBox="0 0 13 13" style="fill: currentcolor;">
                                                <path fill="currentColor" fill-rule="evenodd" d="M6.5 5.793l-2.12-2.12-.708.706 2.12 2.12-2.12 2.12.707.708 2.12-2.12 2.12 2.12.708-.707-2.12-2.12 2.12-2.12-.707-.708-2.12 2.12zM7 13c-4.09 0-7-2.91-7-6 0-4.09 2.91-7 7-7 3.09 0 6 2.91 6 7 0 3.09-2.91 6-6 6z"></path>
                                            </svg>
                                        </section>
                                    </div>
                                    <div class="P2viIkaR1i">
                                        <div class="_2JvYDfXjGf">
                                            <span class="_3Fyhtvkkc_">Your Content</span>
                                            <a class="ATag" href="/category/continue_watching?tracked=1">Continue Watching</a>
                                            <a class="ATag" href="/category/queue?tracked=1">Queue</a>
                                        </div>
                                        <div class="tJjlesOdcq">
                                            <div class="bY5uk64vf0 _2kWuFQpi3c">
                                                <a class="ATag" href="/category/featured?tracked=1">Featured</a>
                                                <a class="ATag" href="/category/most_popular?tracked=1">Most Popular</a>
                                                <a class="ATag" href="/category/not_on_netflix?tracked=1">Not on Netflix</a>
                                                <a class="ATag" href="/category/comedy?tracked=1">Comedy</a>
                                                <a class="ATag" href="/category/horror?tracked=1">Horror</a>
                                                <a class="ATag" href="/category/weekly_watchlist?tracked=1">Weekly Watchlist</a>
                                                <a class="ATag" href="/category/action?tracked=1">Action</a>
                                                <a class="ATag" href="/category/black_cinema?tracked=1">Black Cinema</a>
                                                <a class="ATag" href="/category/family_movies?tracked=1">Family Movies</a>
                                                <a class="ATag" href="/category/holiday_movies?tracked=1">Holiday Movies</a>
                                                <a class="ATag" href="/category/recently_added?tracked=1">Recently Added</a>
                                                <a class="ATag" href="/category/new_releases?tracked=1">New Releases</a>
                                                <a class="ATag" href="/category/drama?tracked=1">Drama</a>
                                                <a class="ATag" href="/category/thrillers?tracked=1">Thrillers</a>
                                                <a class="ATag" href="/category/movie_night?tracked=1">Movie Night!</a>
                                                <a class="ATag" href="/category/sci_fi_and_fantasy?tracked=1">Sci-fi &amp; Fantasy</a>
                                                <a class="ATag" href="/category/leaving_soon?tracked=1">Leaving Soon!</a>
                                                <a class="ATag" href="/category/documentary?tracked=1">Documentary</a>
                                                <a class="ATag" href="/category/romance?tracked=1">Romance</a>
                                                <a class="ATag" href="/category/highly_rated_on_rotten_tomatoes?tracked=1">Highly Rated on Rotten Tomatoes</a>
                                                <a class="ATag" href="/category/reality_tv?tracked=1">Reality TV</a>
                                                <a class="ATag" href="/category/trending?tracked=1">Trending</a>
                                                <a class="ATag" href="/category/martial_arts?tracked=1">Martial Arts</a>
                                                <a class="ATag" href="/channels/lifetime?tracked=1">Lifetime</a>
                                                <a class="ATag" href="/category/cult_favorites?tracked=1">Cult Classics</a>
                                                <a class="ATag" href="/category/kids_shows?tracked=1">Kids Shows</a>
                                                <a class="ATag" href="/category/crime_tv?tracked=1">Crime TV</a>
                                                <a class="ATag" href="/category/lgbt?tracked=1">LGBT</a>
                                                <a class="ATag" href="/category/stand_up_comedy?tracked=1">Stand Up Comedy</a>
                                                <a class="ATag" href="/category/anime?tracked=1">Anime</a>
                                                <a class="ATag" href="/category/foreign_films?tracked=1">Foreign Language Films</a>
                                                <a class="ATag" href="/category/classics?tracked=1">Classics</a>
                                                <a class="ATag" href="/category/foreign_language_tv?tracked=1">Foreign Language TV</a>
                                                <a class="ATag" href="/category/tv_comedies?tracked=1">TV Comedies</a>
                                                <a class="ATag" href="/channels/aetv?tracked=1">A&amp;E</a>
                                                <a class="ATag" href="/category/music_musicals?tracked=1">Music &amp; Musicals</a>
                                                <a class="ATag" href="/category/award_winners_and_nominees?tracked=1">Award Winners &amp; Nominees</a>
                                                <a class="ATag" href="/channels/impact_channel?tracked=1">IMPACT</a>
                                                <a class="ATag" href="/category/tv_dramas?tracked=1">TV Dramas</a>
                                                <a class="ATag" href="/channels/shoutfactory?tracked=1">Shout! Factory TV</a>
                                                <a class="ATag" href="/category/home_and_garden?tracked=1">Home &amp; Garden</a>
                                                <a class="ATag" href="/category/indie_films?tracked=1">Indie Films</a>
                                                <a class="ATag" href="/category/sports_movies_and_tv?tracked=1">Sports Movies and Shows</a>
                                                <a class="ATag" href="/category/lifestyle_tv?tracked=1">Lifestyle</a>
                                                <a class="ATag" href="/category/spanish_language?tracked=1">Spanish Language</a>
                                                <a class="ATag" href="/category/faith_and_spirituality?tracked=1">Faith</a>
                                                <a class="ATag" href="/category/docuseries?tracked=1">Docuseries</a>
                                                <a class="ATag" href="/channels/full_moon_features?tracked=1">Full Moon Features</a>
                                                <a class="ATag" href="/channels/docurama?tracked=1">Docurama</a>
                                                <a class="ATag" href="/channels/contv?tracked=1">CONtv</a>
                                                <a class="ATag" href="/category/preschool?tracked=1">Preschool</a>
                                                <a class="ATag" href="/channels/dovechannel?tracked=1">Dove Channel</a>
                                                <a class="ATag" href="/channels/complexnetworks?tracked=1">Complex Networks</a>
                                                <a class="ATag" href="/channels/babyfirsttv?tracked=1">BabyFirst TV</a>
                                            </div>
                                        </div>
                                    </div>
                                </div></div></div>
                                <a class="_2bni8ZAnmH" href="#">
                                    <h1>
                                        <svg alt="Stream Full Length Series &amp; Movies" class="_2rWrRtG6JR _3GE7hIGkdP _2DIK56Sgkq" preserveAspectRatio="xMidYMid meet" style="fill:currentcolor" viewBox="0 0 213 92">
                                            <path fill="currentcolor" d="M210.873307,26.4543269 L196.542112,26.4543269 L196.542112,89.9669695 C196.542112,91.0984165 197.458796,92.0157644 198.589425,92.0157644 L210.873307,92.0157644 C212.003936,92.0157644 212.920621,91.0984165 212.920621,89.9669695 L212.920621,28.5031219 C212.920621,27.3716749 212.003936,26.4543269 210.873307,26.4543269 M155.595838,75.6254051 C146.550295,75.6254051 139.217329,68.2871339 139.217329,59.2350457 C139.217329,50.1829575 146.550295,42.8446863 155.595838,42.8446863 C164.641382,42.8446863 171.974348,50.1829575 171.974348,59.2350457 C171.974348,68.2871339 164.641382,75.6254051 155.595838,75.6254051 M155.595838,26.4543269 C150.78721,26.4543269 146.222213,27.4920416 142.108648,29.3543962 L142.108136,29.353884 C141.843521,29.4737385 141.555361,29.5362267 141.264643,29.5362267 C140.134014,29.5362267 139.217329,28.6188788 139.217329,27.4874318 L139.217329,3.9175828 C139.217329,2.7861358 138.300644,1.86878787 137.170015,1.86878787 L122.83882,1.86878787 L122.83882,59.2350457 C122.83882,77.339222 137.504239,92.0157644 155.595838,92.0157644 C173.687437,92.0157644 188.352857,77.339222 188.352857,59.2350457 C188.352857,41.1308694 173.687437,26.4543269 155.595838,26.4543269 M48.0811613,85.7971597 L48.0832087,85.7986963 L41.9187472,75.1065478 L41.9151644,75.1085965 C41.3936113,74.2045658 40.2762898,73.8378315 39.3207062,74.2562979 C37.3112678,75.1367675 35.091468,75.6254051 32.7570186,75.6254051 C23.711475,75.6254051 16.3785093,68.2871339 16.3785093,59.2350457 L16.3785093,44.8934812 C16.3785093,43.7620342 17.295194,42.8446863 18.425823,42.8446863 L38.8989596,42.8446863 C40.0295885,42.8446863 40.9462732,41.9273384 40.9462732,40.7958914 L40.9462732,28.5036341 C40.9462732,27.3716749 40.0295885,26.4543269 38.8989596,26.4543269 L18.425823,26.4543269 C17.295194,26.4543269 16.3785093,25.536979 16.3785093,24.405532 L16.3785093,3.9170706 C16.3785093,2.7861358 15.4618246,1.86878787 14.3311956,1.86878787 L0,1.86878787 L0,59.2350457 C0,77.339222 14.6659314,92.0157644 32.7570186,92.0157644 C37.9459351,92.0157644 42.8528341,90.808512 47.2131003,88.6593262 C48.2275443,88.1594202 48.6446844,86.9311676 48.1446281,85.9159898 C48.1251786,85.8755261 48.1036818,85.8360868 48.0811613,85.7971597 M112.602251,26.4543269 L98.2710558,26.4543269 L98.2710558,59.2350457 C98.2710558,68.2871339 90.93809,75.6254051 81.8925465,75.6254051 C72.8470029,75.6254051 65.5140372,68.2871339 65.5140372,59.2350457 L65.5140372,28.5031219 C65.5140372,27.3716749 64.5973525,26.4543269 63.4667235,26.4543269 L49.1355279,26.4543269 L49.1355279,59.2350457 C49.1355279,77.339222 63.8014593,92.0157644 81.8925465,92.0157644 C99.9836336,92.0157644 114.649565,77.339222 114.649565,59.2350457 L114.649565,28.5031219 C114.649565,27.3716749 113.73288,26.4543269 112.602251,26.4543269 M204.731366,1.86878787 C200.208338,1.86878787 196.542112,5.53766738 196.542112,10.0639676 C196.542112,14.5902677 200.208338,18.2591473 204.731366,18.2591473 C209.254394,18.2591473 212.920621,14.5902677 212.920621,10.0639676 C212.920621,5.53766738 209.254394,1.86878787 204.731366,1.86878787" transform="translate(0 -1)"></path>
                                        </svg>
                                    </h1>
                                </a>
                            </div>
                        </div>
                        <section class="yOpR1UpemN wG6fJPwbUY">
                            <svg class="_2rWrRtG6JR _20cwlyjJGT" preserveAspectRatio="xMidYMid meet" style="fill:currentcolor" viewBox="0 0 18.07 18.07">
                                <path fill="currentColor" d="M7.5,13A5.5,5.5,0,1,0,2,7.5,5.5,5.5,0,0,0,7.5,13Zm4.55.46A7.5,7.5,0,1,1,13.46,12l4.31,4.31a1,1,0,1,1-1.41,1.41Z"></path>
                            </svg>
                            <form>
                                <input type="search" class="_31SfFjM4T0" required="" placeholder="Find movies, TV shows and more" value=""/>
                            </form>
                            <svg class="_2rWrRtG6JR _2se6dxbH_B" preserveAspectRatio="xMidYMid meet" style="fill:currentcolor" viewBox="0 0 13 13">
                                <path fill="currentColor" fill-rule="evenodd" d="M6.5 5.793l-2.12-2.12-.708.706 2.12 2.12-2.12 2.12.707.708 2.12-2.12 2.12 2.12.708-.707-2.12-2.12 2.12-2.12-.707-.708-2.12 2.12zM7 13c-4.09 0-7-2.91-7-6 0-4.09 2.91-7 7-7 3.09 0 6 2.91 6 7 0 3.09-2.91 6-6 6z"></path>
                            </svg>
                        </section>
                        <div class="_1Sn36tl1qZ">
                            <div class="_1-UPhMa6ru">
                                <div class="y_hxBCBrRA _1glLOCjf4j">
                                    <div class="_3tRfC2xKrN">
                                        Hi, <span class="_4wVtjR8diV"><?php echo $this->session->userdata('name');?></span>
                                    </div>
                                    <div>
                                        <div class="_2uSV0rio0i">
                                            <a class="ATag activeOnWhite" href="/account">Account Settings</a>
                                            <a href="https://help.tubitv.com/hc/en-us" rel="noopener" target="_blank" class="ATag">Help Center</a>
                                            <a class="ATag" href="/activate">Activate Your Device</a><a class="ATag" href="<?php echo base_url();?>index.php?home/signout">Sign Out</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <div class="_2pU6twUvc">
                    <div class="_1py48i12t6"></div>


                    <div class="_1nirg3Kvc6">
                        <div class="_1Y3WaX6o7B main-slider">
                        </div>

                        <div class="Container _3wxhiAc6N-">
                            <div class="Row _2XGPuzJkQo">
                                <div class="Col Col--12 Col--md-6 _29Ovi5Ot8E">
                                    <div class="OIvoWzYhhL">
                                        <svg class="_2rWrRtG6JR U1NsFrIy-1" preserveAspectRatio="xMidYMid meet" style="fill:currentcolor" viewBox="0 0 64 64">
                                            <circle fill="none" stroke-width="4px" r="30px" cx="32" cy="32"></circle>
                                            <circle fill="currentColor" r="30px" cx="32" cy="32"></circle>
                                            <path d="M28.42,37.6c-2,1-3.42,0-3.42-2.35v-8.5c0-2.34,1.38-3.39,3.42-2.35l9,4.7c2,1,2.11,2.76.07,3.8Z"></path>
                                            <path stroke-width="20%" style="transform:scale(1.429);transform-origin:50%" fill="#FFF" d="M28.42,37.6c-2,1-3.42,0-3.42-2.35v-8.5c0-2.34,1.38-3.39,3.42-2.35l9,4.7c2,1,2.11,2.76.07,3.8Z"></path>
                                        </svg>
                                    </div>
                                    <div class="_2c2HYF-jei">
                                        <div class="_2EQDRG8QQj">
                                            <span class="_33NIKwFiud"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="Row mGIih1cJLv">
                                <ol class="flex-control-nav flex-control-paging">
                                </ol>
                                <!-- <div class="Col Col--lg-4 _3YKicf-App">
                                    <div class="aaBYOijyzO">
                                        <div class="_1G9onSPbtR _2MvxiQhwbI" style="background-image:linear-gradient(to bottom right, rgba(38, 38, 45, 0.7), rgba(38, 38, 45, 0.7)), url(//images.adrise.tv/bytS-oVYlyvy9C9TIywVggHedrI=/400x574/smart/img.adrise.tv/2d0eb535-ff98-44ee-811f-7d93c7393b0d.jpg);cursor:pointer"></div>
                                        <div class="_1G9onSPbtR" style="background-image:url(//images.adrise.tv/tdVi56amFDsKkfOhS5D3m-kKku8=/400x574/smart/img.adrise.tv/6f345df3-0683-4cc1-ac77-ad7af843b0cf.jpg);cursor:default"></div>
                                    </div>
                                    <div class="_3Xnk88Y2VP">
                                        <div class="_1tLWNtTnWu"></div>
                                        <div class="_1fjlnJ0VxU">
                                            <div class="_1_mFMbHcoJ">Comedy</div>
                                            <div class="_1MmGlZp7ys">(2003) </div>
                                        </div>
                                        <div class="PoLdP-uxPf">TV-14</div>
                                    </div>
                                </div>
                                <div class="Col Col--lg-4 _3WSKTxI3Y7">
                                    <a href="/series/2233/green_wing">
                                        <button class="Button Button--large _2SyTXTs-b_">
                                            <div class="Button__bg"></div>
                                            <div class="Button__content">
                                                Watch Now<span class="_2Vow3rck1m">FREE</span>
                                            </div>
                                        </button>
                                    </a>
                                </div>
                                <div class="Col Col--lg-4 _3jpDML25vG">
                                    <div class="aaBYOijyzO bqj5oISTKq">
                                        <div class="_1G9onSPbtR _2MvxiQhwbI" style="background-image:linear-gradient(to bottom right, rgba(38, 38, 45, 0.7), rgba(38, 38, 45, 0.7)), url(//images.adrise.tv/J1QazUb3Un_ZZ9Gu7DNcPeCeq7E=/400x574/smart/img.adrise.tv/13a906fe-ead5-4836-830a-adcee7d65d7d.jpg);cursor:pointer"></div>
                                        <div class="_1G9onSPbtR" style="background-image:linear-gradient(to bottom right, rgba(38, 38, 45, 0.7), rgba(38, 38, 45, 0.7)), url(//images.adrise.tv/CVZx9LQZ6ls4dPg3XA7ytxif7VQ=/400x574/smart/img.adrise.tv/b3567316-a4d0-4ece-a892-abca0b4276d6.jpg);cursor:pointer"></div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="_3WQeSlUjM0">
                            <div id="categoriesList">
                            </div>
                            <div class="Container">
                                <div class="Row _2YqhjboJKE">
                                    <div class="Col Col--sm-4 Col--lg-3 Col--xl-1-5 Col--xxl-2">
                                        <button class="Button Button--large Button--secondary Button--block">
                                            <div class="Button__bg"></div>
                                            <div class="Button__content">Load More</div>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_2IQWzEKDPr">
                        <div class="Container">
                            <div class="Row _20pdU9643c">
                                <div class="_1Ubk_Sfhr1">
                                    <a class="ATag _2CQSo88Ny- active" href="/home">
                                        <svg class="_2rWrRtG6JR _3GE7hIGkdP _3K8H0MhoqZ" preserveAspectRatio="xMidYMid meet" style="fill:currentcolor" viewBox="0 0 213 92">
                                            <path fill="currentcolor" d="M210.873307,26.4543269 L196.542112,26.4543269 L196.542112,89.9669695 C196.542112,91.0984165 197.458796,92.0157644 198.589425,92.0157644 L210.873307,92.0157644 C212.003936,92.0157644 212.920621,91.0984165 212.920621,89.9669695 L212.920621,28.5031219 C212.920621,27.3716749 212.003936,26.4543269 210.873307,26.4543269 M155.595838,75.6254051 C146.550295,75.6254051 139.217329,68.2871339 139.217329,59.2350457 C139.217329,50.1829575 146.550295,42.8446863 155.595838,42.8446863 C164.641382,42.8446863 171.974348,50.1829575 171.974348,59.2350457 C171.974348,68.2871339 164.641382,75.6254051 155.595838,75.6254051 M155.595838,26.4543269 C150.78721,26.4543269 146.222213,27.4920416 142.108648,29.3543962 L142.108136,29.353884 C141.843521,29.4737385 141.555361,29.5362267 141.264643,29.5362267 C140.134014,29.5362267 139.217329,28.6188788 139.217329,27.4874318 L139.217329,3.9175828 C139.217329,2.7861358 138.300644,1.86878787 137.170015,1.86878787 L122.83882,1.86878787 L122.83882,59.2350457 C122.83882,77.339222 137.504239,92.0157644 155.595838,92.0157644 C173.687437,92.0157644 188.352857,77.339222 188.352857,59.2350457 C188.352857,41.1308694 173.687437,26.4543269 155.595838,26.4543269 M48.0811613,85.7971597 L48.0832087,85.7986963 L41.9187472,75.1065478 L41.9151644,75.1085965 C41.3936113,74.2045658 40.2762898,73.8378315 39.3207062,74.2562979 C37.3112678,75.1367675 35.091468,75.6254051 32.7570186,75.6254051 C23.711475,75.6254051 16.3785093,68.2871339 16.3785093,59.2350457 L16.3785093,44.8934812 C16.3785093,43.7620342 17.295194,42.8446863 18.425823,42.8446863 L38.8989596,42.8446863 C40.0295885,42.8446863 40.9462732,41.9273384 40.9462732,40.7958914 L40.9462732,28.5036341 C40.9462732,27.3716749 40.0295885,26.4543269 38.8989596,26.4543269 L18.425823,26.4543269 C17.295194,26.4543269 16.3785093,25.536979 16.3785093,24.405532 L16.3785093,3.9170706 C16.3785093,2.7861358 15.4618246,1.86878787 14.3311956,1.86878787 L0,1.86878787 L0,59.2350457 C0,77.339222 14.6659314,92.0157644 32.7570186,92.0157644 C37.9459351,92.0157644 42.8528341,90.808512 47.2131003,88.6593262 C48.2275443,88.1594202 48.6446844,86.9311676 48.1446281,85.9159898 C48.1251786,85.8755261 48.1036818,85.8360868 48.0811613,85.7971597 M112.602251,26.4543269 L98.2710558,26.4543269 L98.2710558,59.2350457 C98.2710558,68.2871339 90.93809,75.6254051 81.8925465,75.6254051 C72.8470029,75.6254051 65.5140372,68.2871339 65.5140372,59.2350457 L65.5140372,28.5031219 C65.5140372,27.3716749 64.5973525,26.4543269 63.4667235,26.4543269 L49.1355279,26.4543269 L49.1355279,59.2350457 C49.1355279,77.339222 63.8014593,92.0157644 81.8925465,92.0157644 C99.9836336,92.0157644 114.649565,77.339222 114.649565,59.2350457 L114.649565,28.5031219 C114.649565,27.3716749 113.73288,26.4543269 112.602251,26.4543269 M204.731366,1.86878787 C200.208338,1.86878787 196.542112,5.53766738 196.542112,10.0639676 C196.542112,14.5902677 200.208338,18.2591473 204.731366,18.2591473 C209.254394,18.2591473 212.920621,14.5902677 212.920621,10.0639676 C212.920621,5.53766738 209.254394,1.86878787 204.731366,1.86878787" transform="translate(0 -1)"></path>
                                        </svg>
                                    </a>
                                </div>
                                <div class="_23TJ8ZMflr"></div>
                                <div class="Col Col--4 Col--lg-3 Col--xl-1-5 Col--xxl-2 _3O4gjb62-X">
                                    <div class="_3Rq0DG7Zts">
                                        <div class="SPKaS2chiW">
                                            <a href="https://www.facebook.com/tubitv" rel="noopener" target="_blank" class="ATag _2_qmcmATnZ">
                                                <svg class="_2rWrRtG6JR _1-m4hV5yij" preserveAspectRatio="xMidYMid meet" style="fill:currentcolor" viewBox="0 0 20 20">
                                                    <path fill="currentColor" fill-rule="evenodd" d="M2 0C.938 0 0 1.063 0 1.97v16.093C0 19.03 1.063 20 2 20h9v-8H8V9h3V7c-.318-2.573 1.26-3.98 4-4 .668.02 1.617.103 2 0v3h-2c-.957-.16-1.2.436-1 1v2h3l-1 3h-2v8h3.938c1.03 0 2.062-.938 2.062-1.938V1.97C20 1.03 18.937 0 17.937 0H2z"></path>
                                                </svg>
                                            </a>
                                            <a href="https://www.instagram.com/tubitv" rel="noopener" target="_blank" class="ATag Ejsie9BgZZ">
                                                <svg class="_2rWrRtG6JR" preserveAspectRatio="xMidYMid meet" style="fill:currentcolor" viewBox="0 0 20 20">
                                                    <g fill="currentColor" fill-rule="evenodd">
                                                        <path d="M10 0C7.284 0 6.944.012 5.877.06 4.813.11 4.087.278 3.45.525c-.658.256-1.216.598-1.772 1.153C1.123 2.234.78 2.792.525 3.45.278 4.086.11 4.812.06 5.877.012 6.944 0 7.284 0 10s.012 3.057.06 4.123c.05 1.065.218 1.79.465 2.428.256.658.598 1.216 1.153 1.77.556.558 1.114.9 1.772 1.155.636.248 1.363.417 2.427.464 1.067.048 1.407.06 4.123.06s3.057-.012 4.123-.06c1.064-.048 1.79-.217 2.428-.465.658-.255 1.216-.597 1.77-1.154.558-.554.9-1.112 1.155-1.77.248-.636.417-1.362.464-2.427.048-1.066.06-1.407.06-4.123s-.012-3.056-.06-4.123c-.048-1.065-.217-1.79-.465-2.427-.255-.658-.597-1.216-1.154-1.772-.554-.555-1.112-.897-1.77-1.153C15.915.278 15.188.11 14.124.06 13.057.012 12.716 0 10 0m0 2c2.606 0 2.914.01 3.943.057.952.044 1.468.202 1.812.336.455.177.78.39 1.123.73.34.34.552.667.73 1.12.133.346.292.862.335 1.814C17.99 7.087 18 7.394 18 10s-.01 2.914-.057 3.943c-.043.952-.202 1.468-.335 1.812-.178.455-.39.78-.73 1.123-.343.34-.668.552-1.123.73-.344.133-.86.292-1.812.335-1.03.047-1.337.057-3.943.057s-2.914-.01-3.943-.057c-.952-.043-1.468-.202-1.813-.335-.454-.178-.78-.39-1.12-.73-.342-.343-.554-.668-.73-1.123-.135-.344-.293-.86-.337-1.812C2.01 12.913 2 12.606 2 10s.01-2.914.057-3.943c.044-.952.202-1.468.336-1.813.177-.454.39-.78.73-1.12.34-.342.667-.554 1.12-.73.346-.135.862-.293 1.814-.337C7.087 2.01 7.394 2 10 2"></path>
                                                        <path d="M10 13c-1.657 0-3-1.343-3-3 0-1.656 1.343-3 3-3s3 1.344 3 3c0 1.657-1.343 3-3 3m0-8c-2.76 0-5 2.24-5 5s2.24 5 5 5 5-2.24 5-5-2.24-5-5-5m6 0c0 .553-.447 1-1 1-.553 0-1-.447-1-1 0-.553.447-1 1-1 .553 0 1 .447 1 1"></path>
                                                    </g>
                                                </svg>
                                            </a>
                                            <a href="https://twitter.com/tubi" rel="noopener" target="_blank" class="ATag _20ow7jf-SI">
                                                <svg class="_2rWrRtG6JR" preserveAspectRatio="xMidYMid meet" style="fill:currentcolor" viewBox="0 0 20 17">
                                                    <path fill="currentColor" fill-rule="evenodd" d="M6 17c7.837 0 11.965-6.156 12-11-.035-.67-.035-.844 0-1 .756-.59 1.45-1.297 2-2-.75.218-1.543.433-2 1 .5-.978 1.14-1.77 1-3-.358.763-1.24 1.095-2 1C15.29.647 12.69.568 11 2c-1.03 1.084-1.48 2.555-1 4-3.45-.204-6.524-1.74-9-4C.303 3.584.86 5.945 3 7c-.99.11-1.63-.062-2 0-.2 1.6 1.178 3.255 3 4-.512-.202-1.146-.178-2 0 .777 1.35 2.318 2.478 4 3-1.38.635-3.175 1.246-5 1-.35.244-.675.223-1 0 1.877 1.37 4.06 2 6 2"></path>
                                                </svg>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="Row vCrxDrrJ21">
                                <div class="Col Col--4 Col--lg-3 Col--xl-1-5 Col--xxl-2 _1kFxunI3w4"></div>
                                <div class="Col Col--4 Col--lg-3 Col--xl-1-5 Col--xxl-2 _1kFxunI3w4"></div>
                            </div>
                            <div class="Row _3bXfccS9GN">
                                <div class="Col Col--4 Col--lg-3 Col--xl-1-5 Col--xxl-2">
                                    <ul class="_1FwaPZYfiU">
                                        <li class="_1VwYJZoQ1m">COMPANY</li>
                                        <li>
                                            <a class="ATag" href="/static/careers">Careers</a>
                                        </li>
                                        <li>
                                            <a href="http://blog.tubi.tv/press-releases" rel="noopener" target="_blank" class="ATag">News</a>
                                        </li>
                                        <li>
                                            <a class="ATag" href="mailto:pr@tubi.tv">Press Inquiries</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="Col Col--4 Col--lg-3 Col--xl-1-5 Col--xxl-2">
                                    <ul class="_1FwaPZYfiU">
                                        <li class="_1VwYJZoQ1m">SUPPORT</li>
                                        <li>
                                            <a class="ATag" href="/static/support">Contact Support</a>
                                        </li>
                                        <li>
                                            <a href="https://help.tubitv.com/hc/en-us" rel="noopener" target="_self" class="ATag">Help Center</a>
                                        </li>
                                        <li>
                                            <a class="ATag" href="/static/devices">Supported Devices</a>
                                        </li>
                                        <li>
                                            <a class="ATag" href="/activate">Activate Your Device</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="Col Col--4 Col--lg-3 Col--xl-1-5 Col--xxl-2">
                                    <ul class="_1FwaPZYfiU">
                                        <li class="_1VwYJZoQ1m">PARTNERS</li>
                                        <li>
                                            <a href="http://advertising.tubi.tv" rel="noopener" target="_self" class="ATag">Advertise With Us</a>
                                        </li>
                                        <li>
                                            <a class="ATag" href="mailto:partnerships@tubi.tv">Partnerships</a>
                                        </li>
                                        <li>
                                            <a class="ATag" href="mailto:content-submissions@tubi.tv">Content Submission</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="Col Col--4 Col--lg-3 Col--xl-1-5 Col--xxl-2">
                                    <ul class="_1FwaPZYfiU">
                                        <li class="_1VwYJZoQ1m">GET THE APPS</li>
                                        <li>
                                            <a href="https://itunes.apple.com/us/app/tubi-tv-watch-free-movies/id886445756?mt=8" rel="noopener" target="_self" class="ATag">iOS</a>
                                        </li>
                                        <li>
                                            <a href="https://play.google.com/store/apps/details?id=com.tubitv&amp;hl=en" rel="noopener" target="_self" class="ATag">Android</a>
                                        </li>
                                        <li>
                                            <a href="https://channelstore.roku.com/details/41468/tubi-tv" rel="noopener" target="_self" class="ATag">Roku</a>
                                        </li>
                                        <li>
                                            <a href="https://www.amazon.com/Tubi-Inc-Free-Movies-TV/dp/B075NTHVJW" rel="noopener" target="_self" class="ATag">Amazon Fire</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="Col Col--4 Col--lg-3 Col--xl-1-5 Col--xxl-2">
                                    <ul class="_1FwaPZYfiU">
                                        <li class="_1VwYJZoQ1m">LEGAL</li>
                                        <li>
                                            <a class="ATag" href="/static/terms">Terms of Use</a>
                                        </li>
                                        <li>
                                            <a class="ATag" href="/static/privacy">Privacy Policy</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="Col Col--4 Col--lg-3 Col--xl-1-5 Col--xxl-2">
                                    <ul class="_1qFIfUwj6v">
                                        <li>Copyright ©
                                        <!-- -->
                                        2018
                                        <!-- -->
                                        Tubi, Inc.</li>
                                        <li>Tubi is a registered trademark of Tubi, Inc. All rights reserved.</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="_1kXvxMMZIA">
                                <a href="https://itunes.apple.com/us/app/tubi-tv-watch-free-movies/id886445756?mt=8" rel="noopener" target="_blank" class="ATag">
                                    <svg class="_2rWrRtG6JR Z86-JLkcoe" preserveAspectRatio="xMidYMid meet" style="fill:currentcolor" viewBox="0 0 135 40">
                                        <g fill="none" fill-rule="evenodd">
                                            <path stroke="#FFF" d="M4.73 39.5h125.467c2.345 0 4.303-1.92 4.303-4.233V4.727c0-2.314-1.956-4.227-4.303-4.227H4.73C2.397.5.5 2.396.5 4.726v30.54c0 2.33 1.9 4.234 4.23 4.234z" opacity=".1"></path>
                                            <path stroke="#FFF" d="M30.128 19.784c.033 3.863 3.38 5.14 3.42 5.154-.02.09-.524 1.843-1.78 3.623-1.047 1.568-2.145 3.1-3.89 3.128-1.693.04-2.263-.997-4.206-.997-1.96 0-2.568.97-4.193 1.037-1.662.06-2.93-1.673-4.017-3.226-2.17-3.17-3.86-8.935-1.595-12.858 1.098-1.924 3.1-3.163 5.24-3.196 1.663-.034 3.206 1.123 4.228 1.123 1.002 0 2.91-1.385 4.877-1.178.823.024 3.165.325 4.676 2.528-.123.073-2.79 1.64-2.762 4.864zm-3.2-9.478c-.912 1.076-2.37 1.9-3.792 1.793-.186-1.453.534-2.997 1.34-3.94.91-1.068 2.493-1.887 3.758-1.943.166 1.51-.432 2.996-1.306 4.09z"></path>
                                            <path fill="#FFF" d="M53.645 31.504h-2.27l-1.245-3.91h-4.324l-1.185 3.91h-2.21l4.284-13.308h2.646l4.305 13.308zm-3.89-5.55L48.63 22.48c-.12-.355-.342-1.19-.67-2.507h-.04c-.132.566-.343 1.402-.633 2.507l-1.105 3.475h3.573zm14.907.634c0 1.632-.44 2.922-1.323 3.87-.79.842-1.772 1.263-2.943 1.263-1.264 0-2.172-.453-2.725-1.36h-.04v5.054H55.5V25.067c0-1.026-.027-2.08-.08-3.16h1.876l.12 1.522h.04c.71-1.147 1.79-1.72 3.237-1.72 1.132 0 2.077.448 2.833 1.343.758.896 1.136 2.074 1.136 3.535zm-2.172.078c0-.934-.21-1.704-.632-2.31-.46-.632-1.08-.948-1.856-.948-.526 0-1.004.176-1.43.523-.43.35-.71.808-.84 1.374-.066.264-.1.48-.1.65v1.6c0 .698.215 1.287.643 1.768.428.48.984.72 1.668.72.803 0 1.428-.31 1.875-.927.448-.62.672-1.435.672-2.45zm13.21-.078c0 1.632-.442 2.922-1.325 3.87-.79.842-1.77 1.263-2.94 1.263-1.265 0-2.173-.453-2.725-1.36h-.04v5.054h-2.132V25.067c0-1.026-.027-2.08-.08-3.16h1.876l.12 1.522h.04c.71-1.147 1.788-1.72 3.237-1.72 1.132 0 2.077.448 2.835 1.343.755.896 1.134 2.074 1.134 3.535zm-2.173.078c0-.934-.21-1.704-.633-2.31-.46-.632-1.078-.948-1.855-.948-.528 0-1.005.176-1.433.523-.428.35-.707.808-.838 1.374-.066.264-.1.48-.1.65v1.6c0 .698.214 1.287.64 1.768.428.48.984.72 1.67.72.803 0 1.428-.31 1.875-.927.448-.62.672-1.435.672-2.45zm14.512 1.106c0 1.132-.394 2.053-1.183 2.764-.867.777-2.074 1.165-3.625 1.165-1.432 0-2.58-.275-3.45-.828l.495-1.777c.936.566 1.963.85 3.082.85.802 0 1.427-.182 1.876-.544.447-.36.67-.847.67-1.453 0-.54-.184-.995-.553-1.364-.367-.37-.98-.712-1.836-1.03-2.33-.868-3.494-2.14-3.494-3.815 0-1.094.408-1.99 1.225-2.69.814-.698 1.9-1.047 3.258-1.047 1.21 0 2.217.212 3.02.633l-.533 1.738c-.75-.407-1.598-.61-2.547-.61-.75 0-1.336.184-1.756.552-.355.33-.533.73-.533 1.205 0 .526.203.96.61 1.303.356.316 1 .658 1.937 1.027 1.145.46 1.986 1 2.527 1.618.54.616.81 1.387.81 2.307zm7.048-4.264h-2.35v4.66c0 1.184.414 1.776 1.244 1.776.38 0 .697-.033.947-.1l.058 1.62c-.42.157-.973.236-1.658.236-.842 0-1.5-.257-1.975-.77-.473-.514-.71-1.376-.71-2.587v-4.837h-1.4v-1.6h1.4V20.15l2.093-.633v2.39h2.35v1.6zm10.603 3.12c0 1.474-.42 2.685-1.263 3.632-.883.975-2.055 1.46-3.516 1.46-1.407 0-2.528-.466-3.364-1.4-.836-.934-1.254-2.113-1.254-3.534 0-1.487.43-2.705 1.293-3.652.86-.948 2.023-1.422 3.484-1.422 1.407 0 2.54.467 3.395 1.402.818.907 1.226 2.078 1.226 3.513zm-2.21.068c0-.885-.19-1.644-.573-2.277-.447-.767-1.086-1.15-1.914-1.15-.857 0-1.508.384-1.955 1.15-.383.633-.572 1.404-.572 2.316 0 .885.19 1.644.572 2.276.46.766 1.105 1.148 1.936 1.148.814 0 1.453-.39 1.914-1.168.393-.645.59-1.412.59-2.296zm9.14-2.913c-.21-.04-.435-.06-.67-.06-.75 0-1.33.284-1.74.85-.354.5-.532 1.133-.532 1.896v5.034h-2.13l.02-6.574c0-1.106-.028-2.113-.08-3.02h1.856l.078 1.835h.06c.224-.63.58-1.14 1.065-1.52.475-.343.988-.514 1.54-.514.198 0 .376.015.534.04v2.033zm9.536 2.47c0 .38-.025.703-.078.966h-6.396c.025.947.334 1.672.928 2.172.54.447 1.236.67 2.092.67.947 0 1.81-.15 2.588-.453l.334 1.48c-.908.395-1.98.592-3.217.592-1.488 0-2.656-.438-3.506-1.313-.847-.876-1.272-2.05-1.272-3.525 0-1.447.395-2.652 1.186-3.613.828-1.026 1.947-1.54 3.355-1.54 1.382 0 2.43.514 3.14 1.54.563.815.846 1.823.846 3.02zm-2.033-.554c.014-.633-.125-1.18-.414-1.64-.37-.593-.937-.89-1.7-.89-.697 0-1.264.29-1.697.87-.355.46-.566 1.014-.63 1.658h4.44zM49.05 10.01c0 1.176-.353 2.062-1.058 2.657-.653.55-1.58.824-2.783.824-.597 0-1.107-.025-1.534-.077v-6.43c.557-.09 1.157-.137 1.805-.137 1.146 0 2.01.25 2.59.747.653.563.98 1.368.98 2.416zm-1.105.028c0-.763-.202-1.348-.606-1.756-.405-.407-.995-.61-1.772-.61-.33 0-.61.02-.844.067v4.888c.13.02.365.03.708.03.802 0 1.42-.224 1.857-.67.435-.446.655-1.096.655-1.95zm6.964 1c0 .724-.208 1.318-.622 1.784-.434.48-1.01.718-1.727.718-.69 0-1.242-.23-1.653-.69-.41-.458-.615-1.037-.615-1.735 0-.73.21-1.33.635-1.794.424-.464.994-.697 1.712-.697.69 0 1.247.23 1.668.688.4.447.6 1.023.6 1.727zm-1.088.033c0-.434-.094-.807-.28-1.118-.22-.376-.534-.564-.94-.564-.422 0-.742.188-.962.564-.188.31-.28.69-.28 1.138 0 .435.093.808.28 1.12.227.375.543.563.95.563.4 0 .715-.19.94-.574.195-.318.292-.694.292-1.13zm8.943-2.35l-1.475 4.713h-.96l-.61-2.047c-.156-.51-.282-1.02-.38-1.523h-.02c-.09.518-.216 1.025-.378 1.523l-.65 2.047h-.97L55.935 8.72h1.077l.533 2.24c.13.53.235 1.035.32 1.513h.02c.077-.394.206-.896.388-1.503l.67-2.25h.853l.64 2.202c.156.537.282 1.054.38 1.552h.028c.07-.485.178-1.002.32-1.552l.572-2.202h1.03zm5.433 4.713H67.15v-2.7c0-.832-.316-1.248-.95-1.248-.31 0-.562.114-.757.343-.193.23-.29.5-.29.808v2.796h-1.05v-3.366c0-.414-.012-.863-.037-1.35h.92l.05.738h.03c.12-.23.303-.418.542-.57.284-.175.602-.264.95-.264.44 0 .806.142 1.097.427.362.35.543.87.543 1.562v2.823zM71.088 13.433H70.04V6.556h1.048M77.258 11.037c0 .725-.207 1.32-.62 1.785-.435.48-1.01.718-1.728.718-.693 0-1.244-.23-1.654-.69-.41-.458-.615-1.037-.615-1.735 0-.73.212-1.33.636-1.794.424-.464.994-.697 1.71-.697.694 0 1.25.23 1.67.688.4.447.602 1.023.602 1.727zm-1.088.034c0-.434-.094-.807-.28-1.118-.22-.376-.534-.564-.94-.564-.422 0-.742.188-.96.564-.19.31-.282.69-.282 1.138 0 .435.094.808.28 1.12.228.375.544.563.952.563.4 0 .713-.19.94-.574.194-.318.29-.694.29-1.13zm6.16 2.363h-.94l-.08-.543h-.028c-.322.433-.78.65-1.377.65-.445 0-.805-.143-1.076-.427-.247-.258-.37-.58-.37-.96 0-.576.24-1.015.723-1.32.482-.303 1.16-.452 2.033-.445V10.3c0-.62-.326-.93-.98-.93-.464 0-.874.116-1.228.348l-.213-.688c.438-.27.98-.407 1.617-.407 1.232 0 1.85.65 1.85 1.95v1.736c0 .47.023.845.068 1.123zm-1.088-1.62v-.727c-1.156-.02-1.734.297-1.734.95 0 .246.066.43.2.553.136.122.308.183.513.183.23 0 .446-.073.642-.218.197-.146.318-.33.363-.558.01-.05.017-.113.017-.184zm7.043 1.62h-.93l-.05-.757h-.028c-.297.576-.803.864-1.514.864-.568 0-1.04-.223-1.416-.67-.375-.445-.562-1.024-.562-1.735 0-.763.203-1.38.61-1.853.396-.44.88-.66 1.456-.66.634 0 1.077.213 1.33.64h.02V6.556h1.048v5.607c0 .46.012.882.037 1.27zM87.2 11.445v-.786c0-.137-.01-.247-.03-.33-.06-.253-.186-.465-.38-.636-.194-.17-.43-.257-.7-.257-.39 0-.697.155-.922.466-.223.31-.336.708-.336 1.193 0 .466.107.844.322 1.135.227.31.533.466.916.466.344 0 .62-.13.828-.388.202-.24.3-.527.3-.863zm10.048-.408c0 .725-.207 1.32-.62 1.785-.435.48-1.01.718-1.728.718-.69 0-1.242-.23-1.654-.69-.41-.458-.615-1.037-.615-1.735 0-.73.212-1.33.636-1.794.424-.464.994-.697 1.713-.697.69 0 1.247.23 1.667.688.4.447.6 1.023.6 1.727zm-1.086.034c0-.434-.094-.807-.28-1.118-.222-.376-.534-.564-.942-.564-.42 0-.74.188-.96.564-.19.31-.282.69-.282 1.138 0 .435.094.808.28 1.12.228.375.544.563.952.563.4 0 .715-.19.94-.574.194-.318.292-.694.292-1.13zm6.72 2.363h-1.046v-2.7c0-.832-.316-1.248-.95-1.248-.312 0-.563.114-.757.343-.195.23-.292.5-.292.808v2.796h-1.05v-3.366c0-.414-.01-.863-.036-1.35h.92l.05.738h.028c.123-.23.305-.418.543-.57.285-.175.602-.264.95-.264.44 0 .806.142 1.097.427.363.35.543.87.543 1.562v2.823zm7.054-3.93h-1.154v2.29c0 .583.205.874.61.874.19 0 .345-.016.468-.05l.027.796c-.207.078-.48.117-.814.117-.414 0-.736-.126-.97-.378-.233-.252-.35-.676-.35-1.27V9.503h-.688V8.72h.69v-.865l1.026-.31v1.173h1.155v.786zm5.548 3.93h-1.05v-2.68c0-.845-.315-1.268-.948-1.268-.486 0-.818.245-1 .735-.03.103-.05.23-.05.377v2.835h-1.046V6.556h1.047v2.84h.02c.33-.516.803-.774 1.416-.774.434 0 .793.142 1.078.427.356.354.534.882.534 1.58v2.803zm5.723-2.58c0 .188-.014.346-.04.475h-3.142c.014.466.164.82.455 1.067.266.22.61.33 1.03.33.464 0 .888-.074 1.27-.223l.164.728c-.447.194-.973.29-1.582.29-.73 0-1.305-.214-1.72-.644-.42-.43-.626-1.007-.626-1.73 0-.712.193-1.304.582-1.776.406-.504.955-.756 1.648-.756.678 0 1.193.252 1.54.756.282.4.42.895.42 1.483zm-1-.27c.008-.312-.06-.58-.203-.806-.182-.29-.46-.437-.834-.437-.342 0-.62.142-.834.427-.174.227-.277.498-.31.815h2.18z"></path>
                                        </g>
                                    </svg>
                                </a>
                                <a href="https://play.google.com/store/apps/details?id=com.tubitv&amp;hl=en" rel="noopener" target="_blank" class="ATag">
                                    <svg class="_2rWrRtG6JR HGDJdDqIcV" preserveAspectRatio="xMidYMid meet" style="fill:currentcolor" viewBox="0 0 135 40">
                                        <g fill="none" fill-rule="evenodd">
                                            <path stroke="#FFF" d="M130 .5H5C2.526.5.5 2.526.5 5v30c0 2.474 2.026 4.5 4.5 4.5h125c2.474 0 4.5-2.026 4.5-4.5V5c0-2.474-2.026-4.5-4.5-4.5z" opacity=".1"></path>
                                            <path fill="#FFF" d="M47.918 10.743c0 .838-.248 1.505-.745 2.003-.565.592-1.3.888-2.204.888-.867 0-1.604-.3-2.21-.9-.606-.6-.908-1.346-.908-2.234 0-.89.302-1.633.91-2.234.604-.6 1.34-.9 2.207-.9.428 0 .84.084 1.23.25.39.17.704.392.94.67l-.53.53c-.396-.475-.943-.713-1.64-.713-.633 0-1.18.222-1.64.666-.46.444-.69 1.02-.69 1.73s.23 1.286.69 1.73c.46.445 1.007.666 1.64.666.67 0 1.227-.223 1.675-.67.29-.29.458-.695.502-1.215H44.97v-.72h2.906c.028.157.042.307.042.453"></path>
                                            <path stroke="#FFF" stroke-width=".2" d="M47.918 10.743c0 .838-.248 1.505-.745 2.003-.565.592-1.3.888-2.204.888-.867 0-1.604-.3-2.21-.9-.606-.6-.908-1.346-.908-2.234 0-.89.302-1.633.91-2.234.604-.6 1.34-.9 2.207-.9.428 0 .84.084 1.23.25.39.17.704.392.94.67l-.53.53c-.396-.475-.943-.713-1.64-.713-.633 0-1.18.222-1.64.666-.46.444-.69 1.02-.69 1.73s.23 1.286.69 1.73c.46.445 1.007.666 1.64.666.67 0 1.227-.223 1.675-.67.29-.29.458-.695.502-1.215H44.97v-.72h2.906c.028.157.042.307.042.453z"></path>
                                            <path fill="#FFF" d="M52.528 8.237h-2.732v1.902h2.463v.72h-2.464v1.902h2.732v.738h-3.503v-6h3.503"></path>
                                            <path stroke="#FFF" stroke-width=".2" d="M52.528 8.237h-2.732v1.902h2.463v.72h-2.464v1.902h2.732v.738h-3.503v-6h3.503z"></path>
                                            <path fill="#FFF" d="M55.78 13.5h-.772V8.237h-1.676V7.5h4.123v.737H55.78"></path>
                                            <path stroke="#FFF" stroke-width=".2" d="M55.78 13.5h-.772V8.237h-1.676V7.5h4.123v.737H55.78z"></path>
                                            <path fill="#FFF" d="M60.438 13.5h.77v-6h-.77"></path>
                                            <path stroke="#FFF" stroke-width=".2" d="M60.438 13.5h.77v-6h-.77z"></path>
                                            <path fill="#FFF" d="M64.628 13.5h-.77V8.237H62.18V7.5h4.124v.737h-1.676"></path>
                                            <path stroke="#FFF" stroke-width=".2" d="M64.628 13.5h-.77V8.237H62.18V7.5h4.124v.737h-1.676z"></path>
                                            <path fill="#FFF" d="M70.28 12.222c.443.45.986.674 1.63.674.64 0 1.185-.224 1.63-.674.443-.45.666-1.024.666-1.722s-.223-1.273-.667-1.722c-.445-.45-.99-.675-1.63-.675-.644 0-1.187.225-1.63.675-.444.45-.667 1.024-.667 1.722s.223 1.272.666 1.722m3.83.502c-.59.607-1.324.91-2.2.91-.878 0-1.61-.303-2.2-.91-.59-.606-.884-1.347-.884-2.224 0-.877.294-1.62.884-2.225.59-.606 1.322-.91 2.2-.91.87 0 1.602.305 2.195.914.592.608.888 1.348.888 2.22 0 .877-.295 1.618-.884 2.224"></path>
                                            <path stroke="#FFF" stroke-width=".2" d="M70.28 12.222c.443.45.986.674 1.63.674.64 0 1.185-.224 1.63-.674.443-.45.666-1.024.666-1.722s-.223-1.273-.667-1.722c-.445-.45-.99-.675-1.63-.675-.644 0-1.187.225-1.63.675-.444.45-.667 1.024-.667 1.722s.223 1.272.666 1.722zm3.83.502c-.59.607-1.324.91-2.2.91-.878 0-1.61-.303-2.2-.91-.59-.606-.884-1.347-.884-2.224 0-.877.294-1.62.884-2.225.59-.606 1.322-.91 2.2-.91.87 0 1.602.305 2.195.914.592.608.888 1.348.888 2.22 0 .877-.295 1.618-.884 2.224z"></path>
                                            <path fill="#FFF" d="M76.075 13.5v-6h.938l2.916 4.667h.033l-.034-1.156V7.5h.77v6h-.804l-3.05-4.894h-.034l.034 1.157V13.5"></path>
                                            <path stroke="#FFF" stroke-width=".2" d="M76.075 13.5v-6h.938l2.916 4.667h.033l-.034-1.156V7.5h.77v6h-.804l-3.05-4.894h-.034l.034 1.157V13.5z"></path>
                                            <path fill="#FFF" d="M107.436 30.5h1.865V18h-1.864v12.5zm16.807-7.998l-2.14 5.42h-.063l-2.22-5.42h-2.01l3.33 7.575-1.9 4.214h1.947l5.13-11.788h-2.074zM113.66 29.08c-.61 0-1.463-.306-1.463-1.062 0-.964 1.062-1.335 1.98-1.335.818 0 1.205.178 1.703.418-.145 1.16-1.142 1.98-2.22 1.98zm.226-6.852c-1.35 0-2.75.596-3.33 1.914l1.657.692c.354-.692 1.013-.917 1.705-.917.965 0 1.946.58 1.962 1.608v.13c-.338-.194-1.062-.483-1.946-.483-1.786 0-3.603.98-3.603 2.814 0 1.673 1.465 2.75 3.105 2.75 1.254 0 1.947-.562 2.38-1.222h.065v.965h1.802v-4.794c0-2.22-1.658-3.458-3.796-3.458zm-11.532 1.796H99.7V19.74h2.654c1.395 0 2.187 1.153 2.187 2.14 0 .97-.79 2.144-2.186 2.144zm-.048-6.026h-4.47V30.5H99.7v-4.736h2.606c2.068 0 4.1-1.498 4.1-3.883 0-2.384-2.032-3.882-4.1-3.882zm-24.38 11.084c-1.29 0-2.37-1.08-2.37-2.56 0-1.5 1.08-2.595 2.37-2.595 1.27 0 2.27 1.095 2.27 2.594 0 1.483-1 2.562-2.27 2.562zm2.14-5.88h-.063c-.42-.5-1.225-.95-2.24-.95-2.126 0-4.076 1.868-4.076 4.27 0 2.383 1.95 4.236 4.077 4.236 1.014 0 1.82-.45 2.24-.967h.063v.613c0 1.628-.87 2.497-2.27 2.497-1.145 0-1.854-.82-2.144-1.514l-1.627.677c.467 1.127 1.707 2.513 3.77 2.513 2.19 0 4.044-1.29 4.044-4.43v-7.637h-1.773v.692zM83.13 30.5h1.867V18H83.13v12.5zm4.622-4.124c-.048-1.644 1.273-2.48 2.223-2.48.742 0 1.37.37 1.58.9l-3.803 1.58zm5.8-1.418c-.355-.95-1.434-2.707-3.64-2.707-2.192 0-4.013 1.725-4.013 4.255 0 2.384 1.804 4.253 4.22 4.253 1.95 0 3.077-1.192 3.545-1.885l-1.45-.967c-.484.71-1.144 1.176-2.095 1.176-.95 0-1.627-.435-2.06-1.29l5.685-2.35-.193-.484zm-45.308-1.4v1.803h4.317c-.127 1.016-.466 1.757-.98 2.272-.63.63-1.613 1.322-3.336 1.322-2.66 0-4.737-2.143-4.737-4.8 0-2.66 2.078-4.803 4.737-4.803 1.434 0 2.48.565 3.254 1.29l1.273-1.272c-1.08-1.03-2.512-1.82-4.526-1.82-3.642 0-6.703 2.964-6.703 6.605 0 3.64 3.062 6.605 6.704 6.605 1.965 0 3.447-.645 4.607-1.853 1.193-1.192 1.564-2.868 1.564-4.22 0-.42-.032-.806-.097-1.128h-6.073zm11.078 5.524c-1.288 0-2.4-1.063-2.4-2.577 0-1.53 1.112-2.578 2.4-2.578 1.29 0 2.4 1.047 2.4 2.578 0 1.514-1.11 2.577-2.4 2.577zm0-6.83c-2.352 0-4.27 1.788-4.27 4.253 0 2.45 1.918 4.253 4.27 4.253 2.353 0 4.27-1.804 4.27-4.253 0-2.465-1.917-4.254-4.27-4.254zm9.314 6.83c-1.29 0-2.4-1.063-2.4-2.577 0-1.53 1.11-2.578 2.4-2.578 1.29 0 2.4 1.047 2.4 2.578 0 1.514-1.11 2.577-2.4 2.577zm0-6.83c-2.352 0-4.27 1.788-4.27 4.253 0 2.45 1.918 4.253 4.27 4.253 2.352 0 4.27-1.804 4.27-4.253 0-2.465-1.918-4.254-4.27-4.254z"></path>
                                            <path stroke="#FFF" d="M10.935 8.073c-.29.307-.463.785-.463 1.404v22.117c0 .62.172 1.097.463 1.405l.074.07 12.388-12.39v-.29L11.008 8l-.073.073zm12.463 12.282v.29l4.13 4.133.094-.053 4.892-2.78c1.398-.795 1.398-2.093 0-2.888l-4.892-2.78-.094-.052-4.13 4.13z"></path>
                                            <path stroke="#FFF" d="M11.892 33.362c-.383 0-.712-.14-.957-.398L23.398 20.5l4.224 4.225-14.608 8.3c-.402.228-.783.337-1.122.337zm11.506-12.86l4.224-4.225-14.608-8.3c-.402-.23-.782-.337-1.12-.337-.385 0-.714.14-.96.398L23.4 20.5z"></path>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                            <div class="_2PMG5QqeSC">
                                Made with
                                <svg class="_2rWrRtG6JR" preserveAspectRatio="xMidYMid meet" style="fill:currentcolor" viewBox="0 0 10 9">
                                    <path fill="currentColor" fill-rule="evenodd" d="M9.024 2.58C8.88 1.163 7.87 0 6.448 0c-.766 0-1.453.335-1.924.866C4.05.336 3.364 0 2.6 0 1.175 0 .165 1.162.023 2.58c-.11 1.086.132 2.537 1.197 3.91 1.106 1.424 2.946 2.318 2.946 2.318.227.115.48.103.694 0 0 0 1.86-.894 2.967-2.318C8.89 5.117 9.132 3.666 9.024 2.58"></path>
                                </svg>
                                in San Francisco
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <script async="" src="https://www.gstatic.com/cv/js/sender/v1/cast_sender.js?loadCastFramework=1"></script> -->
    <!-- <script charSet="utf-8" src="//d0.tubitv.com/web/dist/manifest.3592c2f4e3df14795189.js"></script> -->
    <!-- <script charSet="utf-8" src="//d0.tubitv.com/web/dist/vendor.88a9a1b5d7336fedf74d.js"></script> -->
    <!-- <script charSet="utf-8" src="//d0.tubitv.com/web/dist/main.b50f341214423368679a.js"></script> -->
</body>
<script>
    $(window).load(function() {
        // $('.flexslider').flexslider({
        //     animation: "slide",
        //     controlsContainer: $(".custom-controls-container"),
        //     customDirectionNav: $(".custom-navigation a")
        // });

        var children = $(".main-slider").children();
        var index = 0;
        setInterval(() => {
            index ++;
            if (index == children.length)   index = 0;

            children.removeClass("active");
            $(children [index]).addClass("active");
        }, 3000);
    });
</script>

<script>
	var prevScrollTop = 0;
	window.onscroll=function(e) {
		var curScrollTop = document.documentElement.scrollTop;
		// console.log(prevScrollTop, curScrollTop);
		if (curScrollTop < prevScrollTop) {
			$("header").removeClass("_3Q4Yibh5Ue");
			if( curScrollTop == 0){
				if (!$("header").hasClass("RwMWOeGUEX"))
					$("header").addClass("RwMWOeGUEX");
			}
			prevScrollTop = curScrollTop;
			return;
		}
		if (curScrollTop > 300) {
			if (!$("header").hasClass("_3Q4Yibh5Ue"))
				$("header").addClass("_3Q4Yibh5Ue");
			$("header").removeClass("RwMWOeGUEX");
		} else {
			$("header").removeClass("_3Q4Yibh5Ue");
		}

		prevScrollTop = curScrollTop;
	}

var totalList = {};
$(function() {
    $.ajax({
        type: "GET",
        url: "assets/frontend/flixer/window.json",
//        dataType: "json",
//        cache: false,
        success: function(data) {
            totalList = data;
            console.log(totalList);
            processImages();
        },
    });

    $(".Ajer5aIfN6").click(function() {
        $("._1JO9cOSNhq").toggleClass("hide");
    });
    // $(document).click(function(e){
    //     var myTarget = $(".Ajer5aIfN6");
    //     var clicked = e.target.className;
    //     console.log(myTarget);
    //     console.log(clicked);
    //     if($.trim(myTarget) != '') {
    //         if($("." + myTarget) != clicked) {
    //             // $("ul.subnav").slideUp('slow').hide();
    //             $("._1JO9cOSNhq").addClass("hide");
    //         }
    //     }
    // });
})


function processImages() {
    processCategory();
}

var mainSliderVideoIds = [];
var mainSliderIndex = 0;
var mainSliderTimerId = null;
var mainSliderTimer1 = null, mainSliderTimer2 = null;

function processCategory() {
    var { categoriesList } = totalList.category;
    var sliderIndex = categoriesList [0];
    mainSliderVideoIds = totalList.category.catChildrenIdMap [sliderIndex];

    setMainSliderWithIds();
    setCategoryListSection();
}

function setMainSliderWithIds() {
    mainSliderVideoIds.forEach(id => $(".flex-control-nav").append(`<li><a href="#"> </a></li>`));
    setMainSliderWithId(mainSliderVideoIds [mainSliderIndex]);
    mainSliderTimerId = window.setTimeout(setMainSliderNext, 3000);

    $(".flex-control-nav a").each((index, element) => {
        $(element).click(function(e) {
            mainSliderIndex = index;

            setMainSliderWithId (mainSliderVideoIds [mainSliderIndex]);
            window.clearTimeout(mainSliderTimerId);
            mainSliderTimerId = window.setTimeout(setMainSliderNext, 3000);

            e.preventDefault();
            return false;
        });
    })
}

function setMainSliderNext() {
    mainSliderIndex ++;
    if (mainSliderIndex == mainSliderVideoIds.length)   mainSliderIndex = 0;

    setMainSliderWithId (mainSliderVideoIds [mainSliderIndex]);
    mainSliderTimerId = window.setTimeout(setMainSliderNext, 3000);
}

function setMainSliderWithId(id) {
    var videoItem = totalList.video.byId[id];

    /*if (!videoItem.hero_images || !videoItem.hero_images [0]) {
        setMainSliderNext();
        return;
    }*/

    //let background = videoItem.hero_images[0];
    let background = ["https://images.pexels.com/photos/210186/pexels-photo-210186.jpeg?cs=srgb&dl=cascade-clouds-cool-wallpaper-210186.jpg&fm=jpg",
                    "http://www.benjacobsenphoto.com/blog/wp-content/uploads/2016/06/ILCE-7RM2-DSC04036.jpg",
                    "http://images.unsplash.com/photo-1532274402911-5a369e4c4bb5?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjEyMDd9",
                    "http://www.queenstown.com/images/sobipro/blog/3852/img_a-winter-wonderland-landscape-photographers-queenstown-tour-1.jpg"][mainSliderIndex % 4];

    var className = "";
    if ($(".main-slider > div").length == 0) className = " active ";

    $(".main-slider").append(`<div style="background-image:linear-gradient(to bottom, rgba(38, 38, 45, 0.0), #26262d), url(${background})" class="wvPYBNp-xj ${className}"></div>`);

    window.clearTimeout(mainSliderTimer1);
    window.clearTimeout(mainSliderTimer2);

    mainSliderTimer1 = window.setTimeout(function() {
        if ($(".main-slider > div").length == 1) {
            $("._33NIKwFiud").html(videoItem.title);
            return;
        }

        $(".main-slider > div").removeClass("active");
        $(".main-slider > div").last().addClass("active");
        $("._33NIKwFiud").html(videoItem.title);

        mainSliderTimer2 = window.setTimeout(function() {
            for (var i = 0; i < $(".main-slider > div").length - 1; i ++)
                $(".main-slider > div").first().remove();
        }, 500);
    }, 500);

    $(".flex-control-nav").children().children("a").removeClass("flex-active");
    $($(".flex-control-nav").children()[mainSliderIndex]).children("a").addClass("flex-active");
}

function setCategoryListSection() {
    var { categoriesList } = totalList.category;

    for (var i = 1; i <= 5; i ++) {
        var categoryId = categoriesList [i];
        addCategorySection(categoryId);
    }
}

function addCategorySection(categoryId) {
    var sectionHtml = makeCategorySectionHtml(categoryId);

    $("#categoriesList").append(sectionHtml);
    $(`#section-${categoryId}`).data("slideIndex", 0);

    $(`#section-${categoryId}`).find(".Carousel__prev").click(function() {
        slideCarousel(categoryId, -1);
    });
    $(`#section-${categoryId}`).find(".Carousel__next").click(function() {
        slideCarousel(categoryId, 1);
    });

    slideCarousel(categoryId, 0);

    var videoItems = $(`#section-${categoryId}`).find(".qMEiZHlfes");
    videoItems.hover(
        function() {

            $(this).addClass("_2nGfwB_0yA");

            // videoItems.children(".add-to-queue").addClass("_3ayUnYoehS");
            $(this).children(".add-to-queue").removeClass("_3ayUnYoehS");
            $(this).children(".add-to-queue").addClass("_23LnYWLkWM");
        },
        function(){
            $(this).removeClass("_2nGfwB_0yA");
            $(this).children(".add-to-queue").addClass("_3ayUnYoehS");
            $(this).children(".add-to-queue").removeClass("_23LnYWLkWM");
            console.log("awerwer");
        }
    );

    // videoItems.blur(function(e) {
    //     videoItems.removeClass("_2nGfwB_0yA");
    //     videoItems.children(".add-to-queue").addClass("_3ayUnYoehS");
    //     console.log("AAA");
    // });
    videoItems.off('hover', function(){
        videoItems.removeClass("_2nGfwB_0yA");
        videoItems.children(".add-to-queue").addClass("_3ayUnYoehS");
        console.log("AAA");
    })
}

function slideCarousel(categoryId, direction) {
    var categoryElement = $(`#section-${categoryId}`);
    var contentRowElement = categoryElement.find(".Carousel__row");

    let slideCount = contentRowElement.children().length;
    var slideIndex = categoryElement.data("slideIndex");
    let slideWidth = $(contentRowElement.children() [0]).prop("clientWidth");
    let onePageSlideCnt = Math.floor(categoryElement.find(".Carousel__row").prop("clientWidth") / slideWidth);

    slideIndex += direction * onePageSlideCnt;
    if (slideIndex < 0) slideIndex = 0;
    if (slideIndex >= slideCount - onePageSlideCnt)   slideIndex = slideCount - onePageSlideCnt;

    contentRowElement.css("transform", `translate3d(-${slideWidth * slideIndex}px, 0, 0)`);

    categoryElement.find(".Carousel__prev").addClass("Carousel__arrow-active");
    categoryElement.find(".Carousel__next").addClass("Carousel__arrow-active");

    if (slideIndex == 0
        || slideCount - onePageSlideCnt <= 0)
        categoryElement.find(".Carousel__prev").removeClass("Carousel__arrow-active")
    if (slideIndex == slideCount - onePageSlideCnt
        || slideCount - onePageSlideCnt <= 0)
        categoryElement.find(".Carousel__next").removeClass("Carousel__arrow-active")
}

function makeCategorySectionHtml(categoryId) {
    const category = totalList.category.catIdMap [categoryId];
    const videoIds = totalList.category.catChildrenIdMap [categoryId];

    if (videoIds.length == 0)   return "";

    var videoSection = "";
    for (var i = 0; i < videoIds.length; i ++) {
        var videoItem = totalList.video.byId[videoIds [i]];
        videoSection += makeVideoSectionHtml(videoItem,categoryId,i);
    }

    return `<section class="_1-rCeZ9Ird" id="section-${categoryId}">
                <div class="_2vKa8oW5Kg"></div>
                <div class="Container _15y0llTJc_">
                    <div class="_1xNEUnbMaM">
                        <div class="_38bukhX51X">
                            <a class="_2hvCxPUSVv" href="/category/most_popular">
                                <h2 class="_1mK3GS-dug">${category.title}</h2>
                            </a>
                        </div>
                        <a href="/category/most_popular">
                            <div class="_37AwVxE5ps">
                                <div class="_3cK3c0Rrgv"></div>
                            </div>
                        </a>
                    </div>
                    <div class="Carousel _2t-hv2KjV6">
                        <button class="Button Button--secondary Button--round Carousel__prev">
                            <div class="Button__bg"></div>
                            <div class="Button__content">
                                <svg viewBox="0 0 12 5" preserveAspectRatio="xMidYMid meet">
                                    <path fill="currentColor" d="M19.5101147,20.9824691 L15.1222085,18.1291511 C14.7625781,17.8609086 14.3044469,18.0239987 14.0989439,18.4934232 C13.8934408,18.9628476 14.0183857,19.5608448 14.3780161,19.8290874 L18.8779198,22.7659425 C18.949842,22.8195882 19.0280761,22.8573102 19.1094446,22.8775766 C19.764809,23.0408078 20.235191,23.0408078 20.8905554,22.8775766 C20.9719239,22.8573102 21.050158,22.8195882 21.1220802,22.7659425 L25.6219839,19.8290874 C25.9816143,19.5608448 26.1065592,18.9628476 25.9010561,18.4934232 C25.6955531,18.0239987 25.2374219,17.8609086 24.8777915,18.1291511 L20.4898853,20.9824691 C20.127802,21.0619724 19.872198,21.0619724 19.5101147,20.9824691 Z" transform="translate(-14 -18)"></path>
                                </svg>
                            </div>
                        </button>
                        <button class="Button Button--secondary Button--round Carousel__next">
                            <div class="Button__bg"></div>
                            <div class="Button__content">
                                <svg viewBox="0 0 12 5" preserveAspectRatio="xMidYMid meet">
                                    <path fill="currentColor" d="M19.5101147,20.9824691 L15.1222085,18.1291511 C14.7625781,17.8609086 14.3044469,18.0239987 14.0989439,18.4934232 C13.8934408,18.9628476 14.0183857,19.5608448 14.3780161,19.8290874 L18.8779198,22.7659425 C18.949842,22.8195882 19.0280761,22.8573102 19.1094446,22.8775766 C19.764809,23.0408078 20.235191,23.0408078 20.8905554,22.8775766 C20.9719239,22.8573102 21.050158,22.8195882 21.1220802,22.7659425 L25.6219839,19.8290874 C25.9816143,19.5608448 26.1065592,18.9628476 25.9010561,18.4934232 C25.6955531,18.0239987 25.2374219,17.8609086 24.8777915,18.1291511 L20.4898853,20.9824691 C20.127802,21.0619724 19.872198,21.0619724 19.5101147,20.9824691 Z" transform="translate(-14 -18)"></path>
                                </svg>
                            </div>
                        </button>
                        <div class="Carousel__prev-mask"></div>
                        <div class="Carousel__next-mask"></div>
                        <div class="Carousel__content">
                            <div class="Row Carousel__row" style="transform:translate3d(0px, 0, 0)">
                                ${videoSection}
                            </div>
                        </div>
                    </div>
                </div>
            </section>`;
}

function makeVideoSectionHtml(videoItem,categoryId,i) {
    //videoItem.posterarts [0]
    let thumbImg = "//images.adrise.tv/flnpnnoschs5KPesIX5JIXytDqA=/400x574/smart/img.adrise.tv/6a22daa7-7c2f-4ba1-bdd8-e4775be786bf.jpg";
    return `<div class="Col Col--4 Col--lg-3 Col--xl-1-5 Col--xxl-2">
                <div class="_1E5mgaKsBM">
                    <div class="qMEiZHlfes">
                        <a class="N_9fSC3kSU" href="<?php echo base_url();?>index.php?home/detail/${categoryId}/${i}">
                            <svg class="_2rWrRtG6JR _2zJlU5i8xi" preserveAspectRatio="xMidYMid meet" style="fill:currentcolor" viewBox="0 0 62 62">
                                <circle r="30" stroke="currentColor" fill="none" stroke-width="2" cx="31" cy="31"></circle>
                                <path fill="currentColor" d="M28.42,37.6c-2,1-3.42,0-3.42-2.35v-8.5c0-2.34,1.38-3.39,3.42-2.35l9,4.7c2,1,2.11,2.76.07,3.8Z"></path>
                            </svg>
                        </a>
                        <div class="Da6xa0pOLl" style="background-image:url(${thumbImg})">
                            <div class="FeNmlCi1P9"></div>
                        </div>
                        <div class="_2PXdllUTAN TFmzr867pO _3ayUnYoehS add-to-queue">
                            <div class="Br5c-3nVXi">
                                <div class="_4Q9OQMLXd2 _3oHBsto1kg _2McVnGkFi8" style="background-image: linear-gradient(rgba(38, 38, 45, 0.5), rgba(38, 38, 45, 0.5)), url(&quot;//images.adrise.tv/8SyfngaOzI3czp09iihLxz8PSMA=/400x574/smart/img.adrise.tv/fe686778-76c1-4295-96d2-569dfeea5ad9.jpg&quot;); background-position: center bottom; background-size: cover;"></div>
                                <div class="_1fZIje5TRz">
                                    <div class="KXm-qd3DnZ">Add to Queue</div>
                                    <noscript></noscript>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section class="_8ys0xni5bV _3b4X97TS6L">
                        <div class="JB9zq5BLFX">
                            <h3>
                                <a class="ATag zIZVdcg8-y" title="${videoItem.title}" href="/movies/367819/titanic_2?tracked=1">${videoItem.title}</a>
                            </h3>
                            <div class="faTDNmNqMD">
                                <div>
                                    <div class="cgy7akjnbz">
                                        <div class="_3BhXbBoZat">(${videoItem.year})</div>
                                        <span class="_84K2z2ofxA">· </span>
                                        <div class="yPcUuFORd9">${durationToString(videoItem.duration)}</div>
                                    </div>
                                    <div class="RmVOoxH9OU _27rH2XPrHE">
                                        <div>${videoItem.tags.join(", ")}</div>
                                    </div>
                                </div>
                                <div>
                                    <div class="_30bN1kfjEf _2x-llfrOLw">${videoItem.ratings [0].value}</div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>`;
}

function durationToString(duration) {
    //duration: second, ex:5400
    var date = new Date(null);
    date.setSeconds(duration); // specify value for SECONDS here
    var timeString = date.toISOString().substr(11, 8);
    return timeString;
}

</script>
